/*
 * Copyright (c) This code is written by Binocla aka Tony Stark and it's strongly recommended send me a $ on BTC wallet: 3JTmgGaHQ53UBdceeV6qKvgbpsC7XKXzQ7
 */

package space.enthropy.repository;

import space.enthropy.models.Teacher;

import java.util.List;
import java.util.Optional;

public interface TeachersRepository {
    List<Teacher> findAllByFirstName(String name);

    List<Teacher> findAllByLastName(String name);

    Optional<Teacher> findById(Integer id);

    Optional<Teacher> findByIdWithCourses(Integer id);

    void save(Teacher account);
}
