/*
 * Copyright (c) This code is written by Binocla aka Tony Stark and it's strongly recommended send me a $ on BTC wallet: 3JTmgGaHQ53UBdceeV6qKvgbpsC7XKXzQ7
 */

package space.enthropy.repository;

import space.enthropy.models.Course;

import java.util.List;
import java.util.Optional;

public interface CoursesRepository {
    List<Course> findAllByName(String name);

    Optional<Course> findById(Integer id);

    Optional<Course> findByIdWithStudents(Integer id);

    void save(Course account);
}
