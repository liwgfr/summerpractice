/*
 * Copyright (c) This code is written by Binocla aka Tony Stark and it's strongly recommended send me a $ on BTC wallet: 3JTmgGaHQ53UBdceeV6qKvgbpsC7XKXzQ7
 */

package space.enthropy.repository;

import space.enthropy.models.Student;

import java.util.List;
import java.util.Optional;

public interface StudentsRepository {
    List<Student> findAllByFirstName(String name);

    List<Student> findAllByLastName(String name);

    Optional<Student> findById(Integer id);

    Optional<Student> findByIdWithCourses(Integer id);

    void save(Student account);
}
