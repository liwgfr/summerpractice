/*
 * Copyright (c) This code is written by Binocla aka Tony Stark and it's strongly recommended send me a $ on BTC wallet: 3JTmgGaHQ53UBdceeV6qKvgbpsC7XKXzQ7
 */

package space.enthropy.repository;

import space.enthropy.models.Lesson;

import java.util.List;
import java.util.Optional;

public interface LessonsRepository {
    List<Lesson> findAllByName(String name);

    Optional<Lesson> findById(Integer id);

    void save(Lesson account);
}
