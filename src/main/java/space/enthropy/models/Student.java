/*
 * Copyright (c) This code is written by Binocla aka Tony Stark and it's strongly recommended send me a $ on BTC wallet: 3JTmgGaHQ53UBdceeV6qKvgbpsC7XKXzQ7
 */

package space.enthropy.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public final class Student {
    private Integer id;
    private final String firstName;
    private final String lastName;
    private final int groupId;
    private List<Course> courses;

    public Student(Integer id, String firstName, String lastName, int groupId) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.groupId = groupId;
    }

    public Student(String firstName, String lastName, int groupId, List<Course> courses) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.groupId = groupId;
        this.courses = courses;
    }
}
