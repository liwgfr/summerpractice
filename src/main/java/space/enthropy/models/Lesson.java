/*
 * Copyright (c) This code is written by Binocla aka Tony Stark and it's strongly recommended send me a $ on BTC wallet: 3JTmgGaHQ53UBdceeV6qKvgbpsC7XKXzQ7
 */

package space.enthropy.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public final class Lesson {
    private Integer id;
    private final String name;
    private final String time;
    private Course course;

    public Lesson(Integer id, String name, String time) {
        this.id = id;
        this.name = name;
        this.time = time;
    }

    public Lesson(String name, String time, Course course) {
        this.name = name;
        this.time = time;
        this.course = course;
    }
}
