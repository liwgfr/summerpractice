/*
 * Copyright (c) This code is written by Binocla aka Tony Stark and it's strongly recommended send me a $ on BTC wallet: 3JTmgGaHQ53UBdceeV6qKvgbpsC7XKXzQ7
 */

package space.enthropy.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.util.List;

@AllArgsConstructor
@RequiredArgsConstructor
@Data
public final class Course {
    private Integer id;
    private final String name;
    private final String date;
    private Teacher teacher;
    private List<Student> students;

    public Course(Integer id, String name, String date) {
        this.id = id;
        this.name = name;
        this.date = date;
    }

    public Course(String name, String date, Teacher teacher) {
        this.name = name;
        this.date = date;
        this.teacher = teacher;
    }
}
