package space.enthropy;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import space.enthropy.impl.CoursesRepositoryJdbcTemplateImpl;
import space.enthropy.impl.LessonsRepositoryJdbcTemplateImpl;
import space.enthropy.impl.StudentsRepositoryJdbcTemplateImpl;
import space.enthropy.impl.TeachersRepositoryJdbcTemplateImpl;
import space.enthropy.models.Course;
import space.enthropy.models.Lesson;
import space.enthropy.models.Student;
import space.enthropy.models.Teacher;
import space.enthropy.repository.CoursesRepository;
import space.enthropy.repository.LessonsRepository;
import space.enthropy.repository.StudentsRepository;
import space.enthropy.repository.TeachersRepository;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

public class Main {

    public static void main(String[] args)  {

        Properties properties = new Properties();
        Scanner in;

        try {
            properties.load(ClassLoader.getSystemResourceAsStream("application.properties"));
        } catch (IOException e) {
            throw new IllegalArgumentException(e);
        }

        HikariConfig config = new HikariConfig();
        config.setDriverClassName(properties.getProperty("db.driver"));
        config.setJdbcUrl(properties.getProperty("db.url"));
        config.setUsername(properties.getProperty("db.user"));
        config.setPassword(properties.getProperty("db.password"));
        config.setMaximumPoolSize(Integer.parseInt(properties.getProperty("db.hikari.pool-size")));

        DataSource dataSource = new HikariDataSource(config);

        CoursesRepository coursesRepository = new CoursesRepositoryJdbcTemplateImpl(dataSource);
        LessonsRepository lessonsRepository = new LessonsRepositoryJdbcTemplateImpl(dataSource);
        StudentsRepository studentsRepository = new StudentsRepositoryJdbcTemplateImpl(dataSource);
        TeachersRepository teachersRepository = new TeachersRepositoryJdbcTemplateImpl(dataSource);

        Course newCourse = new Course("IT", "22.05.1972", new Teacher(1, "Sergey", "Shamov", 2));
        Lesson newLesson = new Lesson("Blabla", "Среда 10:10", newCourse);
        Student newStudent = new Student("Alex", "Black", 11, new ArrayList<>(List.of(newCourse)));
        Teacher newTeacher = new Teacher("Alex", "Black", 2, new ArrayList<>(List.of(newCourse)));
        System.out.println(newCourse);
        System.out.println(newLesson);
        System.out.println(newStudent);
        System.out.println(newTeacher);
        coursesRepository.save(newCourse);
        lessonsRepository.save(newLesson);
        studentsRepository.save(newStudent);
        teachersRepository.save(newTeacher);
        System.out.println(coursesRepository.findAllByName("IT"));
        System.out.println(coursesRepository.findById(2));
        System.out.println(coursesRepository.findByIdWithStudents(3));
        System.out.println(lessonsRepository.findAllByName("Blabla"));
        System.out.println(lessonsRepository.findById(2));
        System.out.println(studentsRepository.findAllByFirstName("Alex"));
        System.out.println(studentsRepository.findAllByLastName("Black"));
        System.out.println(studentsRepository.findById(2));
        System.out.println(teachersRepository.findAllByFirstName("Alex"));
        System.out.println(teachersRepository.findAllByLastName("Black"));
        System.out.println(teachersRepository.findById(2));
    }
}
