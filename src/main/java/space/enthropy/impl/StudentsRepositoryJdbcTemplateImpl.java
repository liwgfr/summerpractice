/*
 * Copyright (c) This code is written by Binocla aka Tony Stark and it's strongly recommended send me a $ on BTC wallet: 3JTmgGaHQ53UBdceeV6qKvgbpsC7XKXzQ7
 */

package space.enthropy.impl;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import space.enthropy.models.Student;
import space.enthropy.repository.StudentsRepository;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class StudentsRepositoryJdbcTemplateImpl implements StudentsRepository {

    private static final String SQL_SELECT_ALL_BY_NAME = "select * from student where firstname = ?";
    private static final String SQL_SELECT_ALL_BY_LAST_NAME = "select * from student where lastname = ?";


    private static final String SQL_UPDATE_BY_ID = "update student set firstname = ?, lastname = ?, groupid = ?, coursesid = ? where id = ?";


    private static final String SQL_SELECT_BY_ID_WITHOUT_COURSES = "select * from student where student.id = ?";
    private static final String SQL_SELECT_BY_ID_WITH_COURSES = "select * from course inner join student on student.coursesid = course.id where student.id = ?";

    private static final String SQL_INSERT = "insert into student(firstname, lastname, groupid, coursesid) values (?, ?, ?, ?)";

    private final JdbcTemplate jdbcTemplate;

    public StudentsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private final RowMapper<Student> studentRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
        String firstName = row.getString("firstName");
        String lastName = row.getString("lastName");
        Integer group = row.getInt("groupid");
        return new Student(id, firstName, lastName, group);
    };

    @Override
    public List<Student> findAllByFirstName(String searchName) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_NAME, studentRowMapper, searchName);
    }

    @Override
    public List<Student> findAllByLastName(String searchName) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_LAST_NAME, studentRowMapper, searchName);
    }


    @Override
    public Optional<Student> findById(Integer id) {
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(SQL_SELECT_BY_ID_WITHOUT_COURSES, studentRowMapper, id));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Student> findByIdWithCourses(Integer id) {
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(SQL_SELECT_BY_ID_WITH_COURSES, studentRowMapper, id));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void save(Student student) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT, new String[]{"id"});
            statement.setString(1, student.getFirstName());
            statement.setString(2, student.getLastName());
            statement.setInt(3, student.getGroupId());
            statement.setInt(4, student.getCourses().get(0).getId());
            return statement;
        }, keyHolder);

        student.setId(Objects.requireNonNull(keyHolder.getKey()).intValue());
    }
}
