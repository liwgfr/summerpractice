/*
 * Copyright (c) This code is written by Binocla aka Tony Stark and it's strongly recommended send me a $ on BTC wallet: 3JTmgGaHQ53UBdceeV6qKvgbpsC7XKXzQ7
 */

package space.enthropy.impl;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import space.enthropy.models.Teacher;
import space.enthropy.repository.TeachersRepository;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class TeachersRepositoryJdbcTemplateImpl implements TeachersRepository {

    private static final String SQL_SELECT_ALL_BY_NAME = "select * from teacher where firstname = ?";
    private static final String SQL_SELECT_ALL_BY_LAST_NAME = "select * from teacher where lastname = ?";


    private static final String SQL_UPDATE_BY_ID = "update teacher set firstname = ?, lastname = ?, experience = ?, coursesid = ? where id = ?";


    private static final String SQL_SELECT_BY_ID_WITHOUT_COURSES = "select * from teacher where teacher.id = ?";
    private static final String SQL_SELECT_BY_ID_WITH_COURSES = "select * from course inner join teacher on teacher.coursesid = course.id where teacher.id = ?";

    private static final String SQL_INSERT = "insert into teacher(firstname, lastname, experience, coursesid) values (?, ?, ?, ?)";

    private final JdbcTemplate jdbcTemplate;

    public TeachersRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private final RowMapper<Teacher> teacherRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
        String firstName = row.getString("firstName");
        String lastName = row.getString("lastName");
        Integer experience = row.getInt("experience");
        return new Teacher(id, firstName, lastName, experience);
    };

    @Override
    public List<Teacher> findAllByFirstName(String searchName) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_NAME, teacherRowMapper, searchName);
    }

    @Override
    public List<Teacher> findAllByLastName(String searchName) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_LAST_NAME, teacherRowMapper, searchName);
    }


    @Override
    public Optional<Teacher> findById(Integer id) {
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(SQL_SELECT_BY_ID_WITHOUT_COURSES, teacherRowMapper, id));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public Optional<Teacher> findByIdWithCourses(Integer id) {
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(SQL_SELECT_BY_ID_WITH_COURSES, teacherRowMapper, id));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void save(Teacher teacher) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT, new String[]{"id"});
            statement.setString(1, teacher.getFirstName());
            statement.setString(2, teacher.getLastName());
            statement.setInt(3, teacher.getExperience());
            statement.setInt(4, teacher.getCourses().get(0).getId());
            return statement;
        }, keyHolder);

        teacher.setId(Objects.requireNonNull(keyHolder.getKey()).intValue());
    }
}
