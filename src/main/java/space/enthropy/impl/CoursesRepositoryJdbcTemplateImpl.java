/*
 * Copyright (c) This code is written by Binocla aka Tony Stark and it's strongly recommended send me a $ on BTC wallet: 3JTmgGaHQ53UBdceeV6qKvgbpsC7XKXzQ7
 */

package space.enthropy.impl;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import space.enthropy.models.Course;
import space.enthropy.repository.CoursesRepository;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class CoursesRepositoryJdbcTemplateImpl implements CoursesRepository {

    private static final String SQL_SELECT_ALL_BY_FIRST_NAME = "select * from course where name = ?";


    private static final String SQL_UPDATE_BY_ID = "update course set name = ?, date = ?, teacher = ? where id = ?";


    private static final String SQL_SELECT_BY_ID_WITHOUT_STUDENTS = "select * from course where course.id = ?";
    private static final String SQL_SELECT_BY_ID_WITH_STUDENTS = "select * from student inner join course on course.id = student.coursesid where course.id = ?";
    // private static final String SQL_SELECT_BY_ID_TEACHER = "select * from teacher where teacher.id = ?";

    private static final String SQL_INSERT = "insert into course(name, date, teacher) values (?, ?, ?)";

    private final JdbcTemplate jdbcTemplate;

    public CoursesRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    /*
    private final RowMapper<Teacher> teacherRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
        String firstName = row.getString("firstName");
        String lastName = row.getString("lastName");
        Integer experience = row.getInt("experience");
        return new Teacher(id, firstName, lastName, experience, new ArrayList<>());
    };
     */
    private final RowMapper<Course> courseRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
        String name = row.getString("name");
        String date = row.getString("date");
        return new Course(id, name, date);
    };

    @Override
    public List<Course> findAllByName(String searchName) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_FIRST_NAME, courseRowMapper, searchName);
    }

    @Override
    public Optional<Course> findById(Integer id) {
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(SQL_SELECT_BY_ID_WITHOUT_STUDENTS, courseRowMapper, id));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }


    @Override
    public Optional<Course> findByIdWithStudents(Integer id) {
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(SQL_SELECT_BY_ID_WITH_STUDENTS, courseRowMapper, id));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void save(Course course) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT, new String[]{"id"});
            statement.setString(1, course.getName());
            statement.setString(2, course.getDate());
            statement.setInt(3, course.getTeacher().getId());
            return statement;
        }, keyHolder);

        course.setId(Objects.requireNonNull(keyHolder.getKey()).intValue());
    }
}
