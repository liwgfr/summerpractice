/*
 * Copyright (c) This code is written by Binocla aka Tony Stark and it's strongly recommended send me a $ on BTC wallet: 3JTmgGaHQ53UBdceeV6qKvgbpsC7XKXzQ7
 */

package space.enthropy.impl;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import space.enthropy.models.Lesson;
import space.enthropy.repository.LessonsRepository;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class LessonsRepositoryJdbcTemplateImpl implements LessonsRepository {

    private static final String SQL_SELECT_ALL_BY_NAME = "select * from lesson where name = ?";


    private static final String SQL_UPDATE_BY_ID = "update lesson set name = ?, time = ?, courseid = ? where id = ?";


    private static final String SQL_SELECT_BY_ID_WITHOUT_COURSES = "select * from lesson where lesson.id = ?";
    private static final String SQL_SELECT_BY_ID_WITH_COURSES = "select * from course inner join lesson on lesson.courseid = course.id where lesson.id = ?";

    private static final String SQL_INSERT = "insert into lesson(name, time, courseid) values (?, ?, ?)";

    private final JdbcTemplate jdbcTemplate;

    public LessonsRepositoryJdbcTemplateImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    private final RowMapper<Lesson> lessonRowMapper = (row, rowNumber) -> {
        int id = row.getInt("id");
        String name = row.getString("name");
        String date = row.getString("time");
        return new Lesson(id, name, date);
    };

    @Override
    public List<Lesson> findAllByName(String searchName) {
        return jdbcTemplate.query(SQL_SELECT_ALL_BY_NAME, lessonRowMapper, searchName);
    }

    @Override
    public Optional<Lesson> findById(Integer id) {
        try {
            return Optional.ofNullable(jdbcTemplate.queryForObject(SQL_SELECT_BY_ID_WITHOUT_COURSES, lessonRowMapper, id));
        } catch (EmptyResultDataAccessException e) {
            return Optional.empty();
        }
    }

    @Override
    public void save(Lesson lesson) {
        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement statement = connection.prepareStatement(SQL_INSERT, new String[]{"id"});
            statement.setString(1, lesson.getName());
            statement.setString(2, lesson.getTime());
            statement.setInt(3, lesson.getCourse().getId());
            return statement;
        }, keyHolder);

        lesson.setId(Objects.requireNonNull(keyHolder.getKey()).intValue());
    }
}
