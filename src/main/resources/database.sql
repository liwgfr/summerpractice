/*
 * Copyright (c) This code is written by Binocla aka Tony Stark and it's strongly recommended send me a $ on BTC wallet: 3JTmgGaHQ53UBdceeV6qKvgbpsC7XKXzQ7
 */

-- создание таблицы с пользователями
create table if not exists course
(
    id      serial primary key,
    name    varchar(20) not null,
    date    varchar(20) not null,
    teacher int         not null
);

create table if not exists lesson
(
    id       serial primary key,
    name     varchar(20) not null,
    time     varchar(20),
    courseId int         not null,
    foreign key (courseId) references course (id)
);
create table if not exists teacher
(
    id         serial primary key,
    firstName  varchar(20) not null,
    lastName   varchar(20) not null,
    experience int,
    coursesId  int         not null,
    foreign key (coursesId) references course (id)
);
create table if not exists student
(
    id        serial primary key,
    firstName varchar(20) not null,
    lastName  varchar(20) not null,
    groupId   int,
    coursesId int,
    foreign key (coursesId) references course (id)
);

-- внесение данных в таблицу с пользователями

insert into course(name, date, teacher)
values ('Java', '10.05.2002', 1);
insert into course(name, date, teacher)
values ('Python', '15.05.2002', 2);

insert into student(firstName, lastName, groupId, coursesId)
values ('Марсель', 'Сидиков', 1, 2);
insert into student(firstName, lastName, groupId, coursesId)
values ('Сергей', 'Сидиков', 2, 1);
insert into student(firstName, lastName, groupId, coursesId)
values ('Марсель', 'Шамов', 1, 2);

insert into teacher(firstName, lastName, experience, coursesId)
values ('Александр', 'Ференец', 10, 2);
insert into teacher(firstName, lastName, experience, coursesId)
values ('Михаил', 'Абрамский', 5, 2);
insert into teacher(firstName, lastName, experience, coursesId)
values ('Владимир', 'Брытков', 7, 1);

insert into lesson(name, time, courseId)
values ('Математика', 'Понедельник 13:45', 2);
insert into lesson(name, time, courseId)
values ('IT', 'Вторник 10:45', 1);
insert into lesson(name, time, courseId)
values ('Engineering', 'Суббота 15:30', 1);
