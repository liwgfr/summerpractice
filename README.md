Readme
------
*Выдача null в courses/teacher/etc - не ошибка.* \
Внутри *impl* есть реализованные SQL запросы для того, чтобы достать необходимое \
null из-за того, что я отправляю такой запрос (в классе прямо видно, что вызывается SQL_...WITHOUT_COURSES/TEACHERS/ETC) \
Проект запакован в jar и билдится с помощью \
mvn clean compile assembly:single \
Реализован CRU (без Delete, насколько я узнал у ребят, его не нужно делать, но могу допилить до полноценного CRUD'a) \
Maven Wrapper для удобства \
Maven Version: 3.8.1 \
Java Version: 16.0.1 (build 16.0.1+9-24, HotSpot Server)
------